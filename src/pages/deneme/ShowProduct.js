import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  Select,
  MenuItem
} from '@material-ui/core';

const kriterler = [
  {
    value: '1',
    label: 'Musteri Hesap No'
  },
  {
    value: '2',
    label: 'Musteri No'
  }
];

const donemler = [
  {
    value: '1',
    label: '202108'
  },
  {
    value: '2',
    label: '202107'
  },
  {
    value: '3',
    label: '202106'
  },
  {
    value: '4',
    label: '202105'
  },
  {
    value: '5',
    label: '202104'
  },
  {
    value: '6',
    label: '202103'
  },
  {
    value: '7',
    label: '202102'
  },
  {
    value: '8',
    label: '202101'
  },
  {
    value: '9',
    label: '202012'
  },
  {
    value: '10',
    label: '202011'
  },
  {
    value: '11',
    label: '202010'
  },
  {
    value: '12',
    label: '202009'
  }
];

const CustomerDetails = (props) => {
  const [kriter, setKriter] = React.useState('1');

  const [values] = useState({
    kriter,
    musteriHesapNo: '',
    musteriNo: '',
    donem: ''
  });

  return (
    <form
      autoComplete="off"
      noValidate
      {...props}
    >
      <Card>
        <CardHeader
          title="Arama Kriteri"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={3}
              xs={12}
            >

              <Select
                fullWidth
                variant="outlined"
                value={kriter}
                onChange={(e) => {
                  setKriter(e.target.value);
                  console.log(e.target.value);
                }}
              >
                {kriterler.map((order) => (
                  <MenuItem value={order.value}>
                    {order.label}
                  </MenuItem>
                ))}
              </Select>

            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              {kriter === '1' ? (
                <TextField
                  fullWidth
                  label="Musteri Hesap No"
                  name="musteriHesapNo"
                  required
                  value={values.musteriHesapNo}
                  variant="outlined"
                />
              ) : (
                <TextField
                  fullWidth
                  label="Musteri No"
                  name="musteriNo"
                  required
                  value={values.musteriNo}
                  variant="outlined"
                />
              )}
            </Grid>

            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                fullWidth
                label="Dönem"
                required
                select
                SelectProps={{ native: true }}
                variant="outlined"
              >
                {donemler.map((option) => (
                  <option
                    key={option.value}
                    value={option.value}
                  >
                    {option.label}
                  </option>
                ))}
              </TextField>
            </Grid>

            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-start',
                p: 4
              }}
            >
              <Button
                color="primary"
                variant="contained"
              >
                Ara
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
    </form>
  );
};

export default CustomerDetails;
