import React, { useEffect } from 'react';
import 'reactjs-popup/dist/index.css';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import axios from 'axios';
import {
  Box,
  Button,
  Card,
  Divider,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const CustomerTable = () => {
  const [data, setData] = React.useState([]);
  const [name, setName] = React.useState([]);
  const [email, setEmail] = React.useState([]);
  const [password, setPassword] = React.useState([]);

  const url = 'http://localhost:8000/api/users';

  const retrieveData = async () => {
    await axios
      .get(url)
      .then((response) => {
        const newData = response.data.data;
        setData(newData);
      }).catch((error) => {
        console.log(error); // Network Error
      });
  };

  useEffect(() => {
    retrieveData();
  }, []);

  const deleteCustomer = async (customerId) => {
    await axios
      .delete(`http://localhost:8000/api/user/${customerId}`)
      .then(() => {
        retrieveData();
      });
  };

  const handleDelete = (e) => {
    deleteCustomer(e);
  };

  const postData = async () => {
    await axios
      .post('http://localhost:8000/api/user/', {
        name,
        email,
        password,
      })
      .then(() => {
        retrieveData();
        setName('');
        setEmail('');
        setPassword('');
      });
  };

  const handleSubmit = () => {
    postData();
  };

  const putData = async (customerId) => {
    await axios
      .put(`http://localhost:8000/api/user/${customerId}`, {
        name,
        email,
        password,
      })
      .then(() => {
        retrieveData();
        setName('');
        setEmail('');
        setPassword('');
      });
  };
  const [open, setOpen] = React.useState(false);
  const [id, setId] = React.useState(1);

  const handleUpdate = (e) => {
    putData(e);
    setOpen(false);
  };

  const handleClickOpen = (rowId) => {
    setId(rowId);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <form
      autoComplete="off"
      noValidate
    >
      {' '}
      <Card>
        <CardHeader
          title="Add Customer"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                fullWidth
                label="Name"
                name="Name"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email"
                name="Email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                fullWidth
                label="Password"
                name="Password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Add
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Card>
        <CardHeader
          title="Customer Table"
        />
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Name</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Password</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.name}</TableCell>
                  <TableCell align="center">{row.email}</TableCell>
                  <TableCell align="center">{row.password}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        UPDATE
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Update Customer"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Name"
                                  name="Name"
                                  value={name}
                                  onChange={(e) => {
                                    setName(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Email"
                                  name="Email"
                                  value={email}
                                  onChange={(e) => {
                                    setEmail(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Password"
                                  name="Password"
                                  value={password}
                                  onChange={(e) => {
                                    setPassword(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                        <DialogActions>
                          <Button onClick={handleClose} color="secondary" variant="contained">
                            Cancel
                          </Button>
                          <Button onClick={() => { handleUpdate(id); }} color="primary" variant="contained">
                            Set
                          </Button>
                        </DialogActions>
                      </Dialog>

                      {' '}
                      <Button
                        onClick={() => { handleDelete(row.id); }}
                        color="secondary"
                        variant="contained"
                      >
                        DELETE
                      </Button>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </form>
  );
};

export default CustomerTable;
