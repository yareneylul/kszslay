import React, { useEffect } from 'react';
import 'reactjs-popup/dist/index.css';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import axios from 'axios';
import {
  Box,
  Button,
  Card,
  Divider,
  CardHeader,
  CardContent,
  TextField,
  Grid,
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { values } from 'lodash';

const CustomerTable = () => {
  const [data, setData] = React.useState([]);
  const [ad, setAd] = React.useState([]);
  const [soyad, setSoyad] = React.useState([]);
  const [tcKimlik, setTckimlik] = React.useState([]);
  const [iletisim, setİletisim] = React.useState([]);
  const [sag, setSag] = React.useState([]);
  const [sartlar, setSartlar] = React.useState([]);
  const [hizmetler, setHizmetler] = React.useState([]);
  const [ziyaret, setZiyaret] = React.useState([]);
  const [talep, setTalep] = React.useState([]);
  const [talepTarih, setTalepTarih] = React.useState([]);
  const [talepAciklama, setTalepAciklama] = React.useState([]);
  const [mezarlıkBilg, setMezarlıkBilg] = React.useState([]);
  const [ziyaretFormu, setZiyaretFormu] = React.useState([]);
  const [foto, setFoto] = React.useState([]);
  const [giderler, setGiderler] = React.useState([]);
  const [sartTarih, setSartTarih] = React.useState([]);
  const [sartAciklama, setSartAciklama] = React.useState([]);
  const [hizmetTarih, setHizmetTarih] = React.useState([]);
  const [hizmetAciklama, setHizmetAciklama] = React.useState([]);
  const [ziyaretTarih, setZiyaretTarih] = React.useState([]);
  const [ziyaretAciklama, setZiyaretAciklama] = React.useState([]);
  const [giderTarih, setGiderTarih] = React.useState([]);
  const [giderAciklama, setGiderAciklama] = React.useState([]);
  const [giderTutar, setGiderTutar] = React.useState([]);
  const url = 'http://localhost:8000/api/bagiscis';
  const retrieveData = async () => {
    await axios
      .get(url)
      .then((response) => {
        const newData = response.data.data;
        setData(newData);
      }).catch((error) => {
        console.log(error); // Network Error
      });
  };

  useEffect(() => {
    retrieveData();
  }, []);
  const durum = [
    {
      value: '1',
      label: 'Sağ'
    },
    {
      value: '2',
      label: 'Ölü'
    }
  ];

  const deleteCustomer = async (customerId) => {
    await axios
      .delete(`http://localhost:8000/api/bagisci/${customerId}`)
      .then(() => {
        retrieveData();
      });
  };

  const handleDelete = (e) => {
    deleteCustomer(e);
  };

  const postData = async () => {
    await axios
      .post('http://localhost:8000/api/bagisci/', {
        ad,
        soyad,
        tcKimlik,
        iletisim,
        sag,
        sartlar,
        hizmetler,
        ziyaret,
        talep,
        talepAciklama,
        talepTarih,
        mezarlıkBilg,
        ziyaretFormu,
        foto,
        giderler,
        sartTarih,
        sartAciklama,
        hizmetAciklama,
        hizmetTarih,
        ziyaretTarih,
        ziyaretAciklama,
        giderAciklama,
        giderTarih,
        giderTutar,
      })
      .then(() => {
        retrieveData();
        setAd('');
        setSoyad('');
        setTckimlik('');
        setİletisim('');
        setSag('');
        setSartlar('');
        setHizmetler('');
        setZiyaret('');
        setTalep('');
        setTalepAciklama('');
        setTalepTarih('');
        setMezarlıkBilg('');
        setZiyaretFormu('');
        setFoto('');
        setGiderler('');
        setSartAciklama('');
        setSartTarih('');
        setHizmetAciklama('');
        setHizmetTarih('');
        setZiyaretAciklama('');
        setZiyaretTarih('');
        setGiderTutar('');
        setGiderTarih('');
        setGiderAciklama('');
      });
  };

  const handleSubmit = () => {
    postData();
  };

  const putData = async (customerId) => {
    await axios
      .put(`http://localhost:8000/api/bagisci/${customerId}`, {
        ad,
        soyad,
        tcKimlik,
        iletisim,
        sag,
        sartlar,
        hizmetler,
        ziyaret,
        talep,
        talepAciklama,
        talepTarih,
        mezarlıkBilg,
        ziyaretFormu,
        foto,
        giderler,
        sartTarih,
        sartAciklama,
        hizmetAciklama,
        hizmetTarih,
        ziyaretTarih,
        ziyaretAciklama,
        giderAciklama,
        giderTarih,
        giderTutar,
      })
      .then(() => {
        retrieveData();
        setAd('');
        setSoyad('');
        setTckimlik('');
        setİletisim('');
        setSag('');
        setSartlar('');
        setHizmetler('');
        setZiyaret('');
        setTalep('');
        setTalepAciklama('');
        setTalepTarih('');
        setMezarlıkBilg('');
        setZiyaretFormu('');
        setFoto('');
        setGiderler('');
        setSartAciklama('');
        setSartTarih('');
        setHizmetAciklama('');
        setHizmetTarih('');
        setZiyaretAciklama('');
        setZiyaretTarih('');
        setGiderTutar('');
        setGiderTarih('');
        setGiderAciklama('');
      });
  };
  const [open, setOpen] = React.useState(false);
  const [id, setId] = React.useState(1);

  const handleUpdate = (e) => {
    putData(e);
    setOpen(false);
  };

  const handleClickOpen = (rowId) => {
    setId(rowId);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <form
      autoComplete="off"
      noValidate
    >
      {' '}
      <Card>
        <CardHeader
          title="Bağışçı Kaydı"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tc Kimlik No"
                name="Tc Kimlik No"
                value={tcKimlik}
                onChange={(e) => {
                  setTckimlik(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="İletişim"
                name="İletişim"
                value={iletisim}
                onChange={(e) => {
                  setİletisim(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Mezarlık Bilgisi"
                name="Mezarlık Bilgisi"
                value={mezarlıkBilg}
                onChange={(e) => {
                  setMezarlıkBilg(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Durumu"
                required
                select
                SelectProps={{ native: true }}
                value={values.durum}
                onChange={(e) => {
                  setSag(e.target.value);
                }}
                variant="outlined"
              >
                {durum.map((option) => (
                  <option
                    key={option.value}
                    value={option.value}
                  >
                    {option.label}
                  </option>
                ))}
              </TextField>
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Kaydet
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Card>
        <CardHeader
          title="Bağışçı Tablosu"
        />
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Tc Kimlik</TableCell>
                <TableCell align="center">İletişim</TableCell>
                <TableCell align="center">Mezarlık Bilgisi</TableCell>
                <TableCell align="center">Durumu</TableCell>
                <TableCell align="center">Ziyaret Formu</TableCell>
                <TableCell align="center">Fotoğraf</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.tcKimlik}</TableCell>
                  <TableCell align="center">{row.iletisim}</TableCell>
                  <TableCell align="center">{row.mezarlıkBilg}</TableCell>
                  <TableCell align="center">{row.sag}</TableCell>
                  <TableCell align="center">{row.ziyaretFormu}</TableCell>
                  <TableCell align="center">{row.foto}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tc Kimlik"
                                  name="Tc Kimlik"
                                  value={tcKimlik}
                                  onChange={(e) => {
                                    setTckimlik(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="İletişim"
                                  name="İletişim"
                                  value={iletisim}
                                  onChange={(e) => {
                                    setİletisim(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Mezarlık Bilgisi"
                                  name="Mezarlık Bilgisi"
                                  value={mezarlıkBilg}
                                  onChange={(e) => {
                                    setMezarlıkBilg(e.target.value);
                                  }}
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Durumu"
                                  value={values.durum}
                                  onChange={(e) => {
                                    setSag(e.target.value);
                                  }}
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ziyaret Formu"
                                  value={values.ziyaretFormu}
                                  onChange={(e) => {
                                    setZiyaretFormu(e.target.value);
                                  }}
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Fotoğraf"
                                  value={values.foto}
                                  onChange={(e) => {
                                    setFoto(e.target.value);
                                  }}
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                        <DialogActions>
                          <Button onClick={handleClose} color="secondary" variant="contained">
                            Cancel
                          </Button>
                          <Button onClick={() => { handleUpdate(id); }} color="primary" variant="contained">
                            Set
                          </Button>
                        </DialogActions>
                      </Dialog>

                      {' '}
                      <Button
                        onClick={() => { handleDelete(row.id); }}
                        color="secondary"
                        variant="contained"
                      >
                        Sil
                      </Button>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Divider />
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
          p: 2
        }}
      >
        <Button
          onClick={handleSubmit}
          color="primary"
          variant="contained"
        >
          Tapu Ekle
        </Button>
      </Box>
      <Card>
        <CardHeader
          title="Talepler"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Talep"
                name="Talep"
                value={talep}
                onChange={(e) => {
                  setTalep(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tarih"
                name="Tarih"
                value={talepTarih}
                onChange={(e) => {
                  setTalepTarih(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Açıklama"
                name="Açıklama"
                value={talepAciklama}
                onChange={(e) => {
                  setTalepAciklama(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Ekle
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Talep</TableCell>
                <TableCell align="center">Tarih</TableCell>
                <TableCell align="center">Açıklama</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.talep}</TableCell>
                  <TableCell align="center">{row.talepTarih}</TableCell>
                  <TableCell align="center">{row.talepAciklama}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Talep"
                                  name="Talep"
                                  value={talep}
                                  onChange={(e) => {
                                    setTalep(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tarih"
                                  name="Tarih"
                                  value={talepTarih}
                                  onChange={(e) => {
                                    setTalepTarih(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Açıklama"
                                  name="Açıklama"
                                  value={talepAciklama}
                                  onChange={(e) => {
                                    setTalepAciklama(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Dialog>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Card>
        <CardHeader
          title="Şartlar"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Şart"
                name="Şart"
                value={sartlar}
                onChange={(e) => {
                  setSartlar(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tarih"
                name="Tarih"
                value={sartTarih}
                onChange={(e) => {
                  setSartTarih(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Açıklama"
                name="Açıklama"
                value={sartAciklama}
                onChange={(e) => {
                  setSartAciklama(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Ekle
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Şart</TableCell>
                <TableCell align="center">Tarih</TableCell>
                <TableCell align="center">Açıklama</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.sartlar}</TableCell>
                  <TableCell align="center">{row.sartTarih}</TableCell>
                  <TableCell align="center">{row.sartAciklama}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Şart"
                                  name="Şart"
                                  value={sartlar}
                                  onChange={(e) => {
                                    setSartlar(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tarih"
                                  name="Tarih"
                                  value={sartTarih}
                                  onChange={(e) => {
                                    setSartTarih(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Açıklama"
                                  name="Açıklama"
                                  value={sartAciklama}
                                  onChange={(e) => {
                                    setSartAciklama(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Dialog>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Card>
        <CardHeader
          title="Hizmetler"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Hizmet"
                name="Hizmet"
                value={hizmetler}
                onChange={(e) => {
                  setHizmetler(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tarih"
                name="Tarih"
                value={hizmetTarih}
                onChange={(e) => {
                  setHizmetTarih(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Açıklama"
                name="Açıklama"
                value={hizmetAciklama}
                onChange={(e) => {
                  setHizmetAciklama(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Ekle
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Hizmet</TableCell>
                <TableCell align="center">Tarih</TableCell>
                <TableCell align="center">Açıklama</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.hizmetler}</TableCell>
                  <TableCell align="center">{row.hizmetTarih}</TableCell>
                  <TableCell align="center">{row.hizmetAciklama}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Hizmet"
                                  name="Hizmet"
                                  value={hizmetler}
                                  onChange={(e) => {
                                    setHizmetler(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tarih"
                                  name="Tarih"
                                  value={hizmetTarih}
                                  onChange={(e) => {
                                    setHizmetTarih(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Açıklama"
                                  name="Açıklama"
                                  value={hizmetAciklama}
                                  onChange={(e) => {
                                    setHizmetAciklama(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Dialog>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Card>
        <CardHeader
          title="Ziyaretler"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ziyaret"
                name="Ziyaret"
                value={ziyaret}
                onChange={(e) => {
                  setZiyaret(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tarih"
                name="Tarih"
                value={ziyaretTarih}
                onChange={(e) => {
                  setZiyaretTarih(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Açıklama"
                name="Açıklama"
                value={ziyaretAciklama}
                onChange={(e) => {
                  setZiyaretAciklama(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Ekle
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Ziyaret</TableCell>
                <TableCell align="center">Tarih</TableCell>
                <TableCell align="center">Açıklama</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.ziyaret}</TableCell>
                  <TableCell align="center">{row.ziyaretTarih}</TableCell>
                  <TableCell align="center">{row.ziyaretAciklama}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ziyaret"
                                  name="Ziyaret"
                                  value={ziyaret}
                                  onChange={(e) => {
                                    setZiyaret(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tarih"
                                  name="Tarih"
                                  value={ziyaretTarih}
                                  onChange={(e) => {
                                    setZiyaretTarih(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Açıklama"
                                  name="Açıklama"
                                  value={ziyaretAciklama}
                                  onChange={(e) => {
                                    setZiyaretAciklama(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Dialog>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Card>
        <CardHeader
          title="Giderler"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Ad"
                name="Ad"
                value={ad}
                onChange={(e) => {
                  setAd(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Soyad"
                name="Soyad"
                value={soyad}
                onChange={(e) => {
                  setSoyad(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Gider"
                name="Gider"
                value={giderler}
                onChange={(e) => {
                  setGiderler(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tarih"
                name="Tarih"
                value={giderTarih}
                onChange={(e) => {
                  setGiderTarih(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Açıklama"
                name="Açıklama"
                value={giderAciklama}
                onChange={(e) => {
                  setGiderAciklama(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={2}
              xs={12}
            >
              <TextField
                fullWidth
                label="Tutar"
                name="Tutar"
                value={giderTutar}
                onChange={(e) => {
                  setGiderTutar(e.target.value);
                }}
                variant="outlined"
              />
            </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                p: 4
              }}
            >
              <Button
                onClick={handleSubmit}
                color="primary"
                variant="contained"
              >
                Ekle
              </Button>
            </Box>
          </Grid>
        </CardContent>
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">Ad</TableCell>
                <TableCell align="center">Soyad</TableCell>
                <TableCell align="center">Gider</TableCell>
                <TableCell align="center">Tutar</TableCell>
                <TableCell align="center">Tarih</TableCell>
                <TableCell align="center">Açıklama</TableCell>
                <TableCell align="center"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.ad}</TableCell>
                  <TableCell align="center">{row.soyad}</TableCell>
                  <TableCell align="center">{row.giderler}</TableCell>
                  <TableCell align="center">{row.giderTutar}</TableCell>
                  <TableCell align="center">{row.giderTarih}</TableCell>
                  <TableCell align="center">{row.giderAciklama}</TableCell>
                  <TableCell align="center">
                    <div>
                      <Button variant="contained" color="primary" onClick={() => { handleClickOpen(row.id); }}>
                        Güncelle
                      </Button>
                      <Dialog
                        open={open}
                        onClose={handleClose}
                      >
                        <Card>
                          <CardHeader
                            title="Güncelle"
                          />
                          <Divider />
                          <CardContent>
                            <Grid
                              container
                              spacing={3}
                            >
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Ad"
                                  name="Ad"
                                  value={ad}
                                  onChange={(e) => {
                                    setAd(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Soyad"
                                  name="Soyad"
                                  value={soyad}
                                  onChange={(e) => {
                                    setSoyad(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Gider"
                                  name="Gider"
                                  value={giderler}
                                  onChange={(e) => {
                                    setGiderler(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tutar"
                                  name="Tutar"
                                  value={giderTutar}
                                  onChange={(e) => {
                                    setGiderTutar(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Tarih"
                                  name="Tarih"
                                  value={giderTarih}
                                  onChange={(e) => {
                                    setGiderTarih(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid
                                item
                                md={4}
                                xs={12}
                              >
                                <TextField
                                  fullWidth
                                  label="Açıklama"
                                  name="Açıklama"
                                  value={giderAciklama}
                                  onChange={(e) => {
                                    setGiderAciklama(e.target.value);
                                  }}
                                  required
                                  variant="outlined"
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Dialog>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </form>
  );
};

export default CustomerTable;
